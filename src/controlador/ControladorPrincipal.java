package controlador;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import modelo.ModeloEstadisticas;
import modelo.ModeloFecha;
import modelo.ModeloFunciones;
import modelo.ModeloJuego;
import modelo.ModeloLlenarTableroDown;
import modelo.ModeloLlenarTableroUp;
import modelo.ModeloSudoku;
import modelo.ModeloTablero;
import vista.AcercaDe;
import vista.Cronometro;
import vista.Estadisticas;
import vista.Principal;

public class ControladorPrincipal implements ActionListener{
	
	private Principal vista = null;
	private ModeloFunciones funciones = null;
	private ModeloEstadisticas estadisticas = null;
	private int apuntador = 0;
	private int dificultad = 1;
	private ModeloTablero tablero = null;
	private int[][] tableroInicial = null;
	private int[] cerosIniciales = null;
	
	public ControladorPrincipal(Principal principal){
		super();
		this.vista = principal;
		this.funciones = new ModeloFunciones();
		this.tablero = new ModeloTablero(); 
		this.tableroInicial = new int[9][9];
		this.estadisticas = new ModeloEstadisticas();
	}

	public void actionPerformed(ActionEvent ae) {
		
		/********** M E N U **********/
		if(ae.getActionCommand().equalsIgnoreCase("Estadisticas")){
			if(vista.getHerramientas().getCronometro().getResultado() > 0){
				vista.getHerramientas().getCronometro().control(Cronometro.DETENER);
				vista.getHerramientas().getPausa().setText("Continuar");
				controlPausa(false);
			}			
			new Estadisticas();
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("Acerca de")){
			new AcercaDe((Frame)vista);
		}
		
		
		/********** H E R R A M I E N T A S **********/
		if(ae.getActionCommand().equalsIgnoreCase("1")){
			if(apuntador == 1){
				apuntador = 0;
				vista.getHerramientas().getB1().setSelected(false);
			}else {
				apuntador = 1;
				vista.getHerramientas().getB1().setSelected(true);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(false);
			}
			
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("2")){
			if(apuntador == 2){
				apuntador = 0;
				vista.getHerramientas().getB2().setSelected(false);
			}else {
				apuntador = 2;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(true);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(false);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("3")){
			if(apuntador == 3){
				apuntador = 0;
				vista.getHerramientas().getB3().setSelected(false);
			}else {
				apuntador = 3;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(true);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(false);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("4")){
			if(apuntador == 4){
				apuntador = 0;
				vista.getHerramientas().getB4().setSelected(false);
			}else {
				apuntador = 4;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(true);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(false);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("5")){
			if(apuntador == 5){
				apuntador = 0;
				vista.getHerramientas().getB5().setSelected(false);
			}else {
				apuntador = 5;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(true);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(false);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("6")){
			if(apuntador == 6){
				apuntador = 0;
				vista.getHerramientas().getB6().setSelected(false);
			}else {
				apuntador = 6;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(true);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(false);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("7")){
			if(apuntador == 7){
				apuntador = 0;
				vista.getHerramientas().getB7().setSelected(false);
			}else {
				apuntador = 7;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(true);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(false);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("8")){
			if(apuntador == 8){
				apuntador = 0;
				vista.getHerramientas().getB8().setSelected(false);
			}else {
				apuntador = 8;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(true);
				vista.getHerramientas().getB9().setSelected(false);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("9")){
			if(apuntador == 9){
				apuntador = 0;
				vista.getHerramientas().getB9().setSelected(false);
			}else {
				apuntador = 9;
				vista.getHerramientas().getB1().setSelected(false);
				vista.getHerramientas().getB2().setSelected(false);
				vista.getHerramientas().getB3().setSelected(false);
				vista.getHerramientas().getB4().setSelected(false);
				vista.getHerramientas().getB5().setSelected(false);
				vista.getHerramientas().getB6().setSelected(false);
				vista.getHerramientas().getB7().setSelected(false);
				vista.getHerramientas().getB8().setSelected(false);
				vista.getHerramientas().getB9().setSelected(true);
			}
		}
		
		if(ae.getActionCommand().equals(vista.getHerramientas().getDificultadFacil().getActionCommand())){
			vista.getHerramientas().getDificultadFacil().setSelected(true);
			vista.getHerramientas().getDificultadMedio().setSelected(false);
			vista.getHerramientas().getDificultadDificil().setSelected(false);
			vista.getHerramientas().getDificultadMuyDificil().setSelected(false);
			dificultad = (ModeloEstadisticas.FACIL);
		}
		
		if(ae.getActionCommand().equals(vista.getHerramientas().getDificultadMedio().getActionCommand())){
			vista.getHerramientas().getDificultadFacil().setSelected(false);
			vista.getHerramientas().getDificultadMedio().setSelected(true);
			vista.getHerramientas().getDificultadDificil().setSelected(false);
			vista.getHerramientas().getDificultadMuyDificil().setSelected(false);
			dificultad = (ModeloEstadisticas.MEDIO);
		}
		
		if(ae.getActionCommand().equals(vista.getHerramientas().getDificultadDificil().getActionCommand())){
			vista.getHerramientas().getDificultadFacil().setSelected(false);
			vista.getHerramientas().getDificultadMedio().setSelected(false);
			vista.getHerramientas().getDificultadDificil().setSelected(true);
			vista.getHerramientas().getDificultadMuyDificil().setSelected(false);
			dificultad = (ModeloEstadisticas.DIFICIL);
		}
		
		if(ae.getActionCommand().equals(vista.getHerramientas().getDificultadMuyDificil().getActionCommand())){
			vista.getHerramientas().getDificultadFacil().setSelected(false);
			vista.getHerramientas().getDificultadMedio().setSelected(false);
			vista.getHerramientas().getDificultadDificil().setSelected(false);
			vista.getHerramientas().getDificultadMuyDificil().setSelected(true);
			dificultad = (ModeloEstadisticas.MUYDIFICIL);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("Revisar")){
			vista.getHerramientas().getCronometro().control(Cronometro.DETENER);
			if(funciones.revisarJugada(tablero)){
				vista.getHerramientas().getPausa().setEnabled(false);
				vista.getHerramientas().getRevisar().setEnabled(false);
				vista.getHerramientas().getReiniciar().setEnabled(false);
				JOptionPane.showMessageDialog(null, "Jugada Perfecta","Felicidades",JOptionPane.PLAIN_MESSAGE);
				ModeloJuego juego = new ModeloJuego();
				juego.setFecha(new ModeloFecha().toString());
				juego.setPuntuacion(vista.getHerramientas().getCronometro().getResultado());
				if(estadisticas.nuevoRecord(juego, dificultad)){
					String nombreJugdor = JOptionPane.showInputDialog(null, "Introduzca un nombre para\npublicar en la tabla de estdisticas","Nuevo record",JOptionPane.PLAIN_MESSAGE);
					juego.setNombre((nombreJugdor != null)?nombreJugdor:"Anonimo");
					System.out.println(juego);
					estadisticas.set(juego, dificultad);
					new Estadisticas();
				}
			}
			else{
				JOptionPane.showMessageDialog(null, "Jugada Incorrecta","Error Fatal",JOptionPane.ERROR_MESSAGE);
				vista.getHerramientas().getCronometro().control(Cronometro.INICIAR);
			}
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("Nuevo")){
			vista.borrarTablero();
			tableroInicial = new ModeloSudoku().getTablero();
			if(vista.getHerramientas().getDificultadFacil().isSelected()) cerosIniciales = new int[44];
			if(vista.getHerramientas().getDificultadMedio().isSelected()) cerosIniciales = new int[46];
			if(vista.getHerramientas().getDificultadDificil().isSelected()) cerosIniciales = new int[48];
			if(vista.getHerramientas().getDificultadMuyDificil().isSelected()) cerosIniciales = new int[50];
			cerosIniciales = funciones.generarPartida(cerosIniciales.length);
			tablero.setTablero(funciones.marcarJugadas(tableroInicial, cerosIniciales));
			vista.getHerramientas().getCronometro().control(Cronometro.DETENER);
			vista.getHerramientas().getCronometro().control(Cronometro.RESTABLECER);
			vista.getHerramientas().getNuevo().setEnabled(false);
			vista.getHerramientas().getReiniciar().setEnabled(false);
			vista.getHerramientas().getPausa().setEnabled(false);
	  		new ModeloLlenarTableroUp(vista,tablero.getTablero());
			new ModeloLlenarTableroDown(vista,tablero.getTablero());
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("Reiniciar")){
			vista.borrarTablero();
			tablero.setTablero(funciones.marcarJugadas(tableroInicial, cerosIniciales));
			vista.getHerramientas().getCronometro().control(Cronometro.DETENER);
			vista.getHerramientas().getCronometro().control(Cronometro.RESTABLECER);
			vista.getHerramientas().getNuevo().setEnabled(false);
			vista.getHerramientas().getReiniciar().setEnabled(false);
			vista.getHerramientas().getPausa().setEnabled(false);
	  		new ModeloLlenarTableroUp(vista,tablero.getTablero());
			new ModeloLlenarTableroDown(vista,tablero.getTablero());
		}

		if(ae.getActionCommand().equalsIgnoreCase("Pausar")){
			vista.getHerramientas().getCronometro().control(Cronometro.DETENER);
			vista.getHerramientas().getPausa().setText("Continuar");
			controlPausa(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("Continuar")){
			vista.getTablero().setVisible(true);
			vista.getHerramientas().getPausa().setText("Pausar");
			controlPausa(true);
			vista.getHerramientas().getCronometro().control(Cronometro.INICIAR);
		}

		
		/********** T A B L E R O **********/
		
		if(ae.getActionCommand().equalsIgnoreCase("A1")){
			funciones.marcarBoton(vista.getTablero().getA1(), apuntador);
			tablero.getTablero()[0][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero())) vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A2")){
			funciones.marcarBoton(vista.getTablero().getA2(), apuntador);
			tablero.getTablero()[0][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A3")){
			funciones.marcarBoton(vista.getTablero().getA3(), apuntador);
			tablero.getTablero()[0][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A4")){
			funciones.marcarBoton(vista.getTablero().getA4(), apuntador);
			tablero.getTablero()[0][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A5")){
			funciones.marcarBoton(vista.getTablero().getA5(), apuntador);
			tablero.getTablero()[0][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A6")){
			funciones.marcarBoton(vista.getTablero().getA6(), apuntador);
			tablero.getTablero()[0][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A7")){
			funciones.marcarBoton(vista.getTablero().getA7(), apuntador);
			tablero.getTablero()[0][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A8")){
			funciones.marcarBoton(vista.getTablero().getA8(), apuntador);
			tablero.getTablero()[0][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("A9")){
			funciones.marcarBoton(vista.getTablero().getA9(), apuntador);
			tablero.getTablero()[0][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B1")){
			funciones.marcarBoton(vista.getTablero().getB1(), apuntador);
			tablero.getTablero()[1][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B2")){
			funciones.marcarBoton(vista.getTablero().getB2(), apuntador);
			tablero.getTablero()[1][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B3")){
			funciones.marcarBoton(vista.getTablero().getB3(), apuntador);
			tablero.getTablero()[1][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B4")){
			funciones.marcarBoton(vista.getTablero().getB4(), apuntador);
			tablero.getTablero()[1][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B5")){
			funciones.marcarBoton(vista.getTablero().getB5(), apuntador);
			tablero.getTablero()[1][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B6")){
			funciones.marcarBoton(vista.getTablero().getB6(), apuntador);
			tablero.getTablero()[1][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B7")){
			funciones.marcarBoton(vista.getTablero().getB7(), apuntador);
			tablero.getTablero()[1][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B8")){
			funciones.marcarBoton(vista.getTablero().getB8(), apuntador);
			tablero.getTablero()[1][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("B9")){
			funciones.marcarBoton(vista.getTablero().getB9(), apuntador);
			tablero.getTablero()[1][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C1")){
			funciones.marcarBoton(vista.getTablero().getC1(), apuntador);
			tablero.getTablero()[2][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C2")){
			funciones.marcarBoton(vista.getTablero().getC2(), apuntador);
			tablero.getTablero()[2][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C3")){
			funciones.marcarBoton(vista.getTablero().getC3(), apuntador);
			tablero.getTablero()[2][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C4")){
			funciones.marcarBoton(vista.getTablero().getC4(), apuntador);
			tablero.getTablero()[2][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C5")){
			funciones.marcarBoton(vista.getTablero().getC5(), apuntador);
			tablero.getTablero()[2][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C6")){
			funciones.marcarBoton(vista.getTablero().getC6(), apuntador);
			tablero.getTablero()[2][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C7")){
			funciones.marcarBoton(vista.getTablero().getC7(), apuntador);
			tablero.getTablero()[2][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C8")){
			funciones.marcarBoton(vista.getTablero().getC8(), apuntador);
			tablero.getTablero()[2][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("C9")){
			funciones.marcarBoton(vista.getTablero().getC9(), apuntador);
			tablero.getTablero()[2][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D1")){
			funciones.marcarBoton(vista.getTablero().getD1(), apuntador);
			tablero.getTablero()[3][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D2")){
			funciones.marcarBoton(vista.getTablero().getD2(), apuntador);
			tablero.getTablero()[3][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D3")){
			funciones.marcarBoton(vista.getTablero().getD3(), apuntador);
			tablero.getTablero()[3][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D4")){
			funciones.marcarBoton(vista.getTablero().getD4(), apuntador);
			tablero.getTablero()[3][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D5")){
			funciones.marcarBoton(vista.getTablero().getD5(), apuntador);
			tablero.getTablero()[3][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D6")){
			funciones.marcarBoton(vista.getTablero().getD6(), apuntador);
			tablero.getTablero()[3][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D7")){
			funciones.marcarBoton(vista.getTablero().getD7(), apuntador);
			tablero.getTablero()[3][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D8")){
			funciones.marcarBoton(vista.getTablero().getD8(), apuntador);
			tablero.getTablero()[3][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("D9")){
			funciones.marcarBoton(vista.getTablero().getD9(), apuntador);
			tablero.getTablero()[3][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E1")){
			funciones.marcarBoton(vista.getTablero().getE1(), apuntador);
			tablero.getTablero()[4][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E2")){
			funciones.marcarBoton(vista.getTablero().getE2(), apuntador);
			tablero.getTablero()[4][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E3")){
			funciones.marcarBoton(vista.getTablero().getE3(), apuntador);
			tablero.getTablero()[4][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E4")){
			funciones.marcarBoton(vista.getTablero().getE4(), apuntador);
			tablero.getTablero()[4][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E5")){
			funciones.marcarBoton(vista.getTablero().getE5(), apuntador);
			tablero.getTablero()[4][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E6")){
			funciones.marcarBoton(vista.getTablero().getE6(), apuntador);
			tablero.getTablero()[4][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E7")){
			funciones.marcarBoton(vista.getTablero().getE7(), apuntador);
			tablero.getTablero()[4][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E8")){
			funciones.marcarBoton(vista.getTablero().getE8(), apuntador);
			tablero.getTablero()[4][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("E9")){
			funciones.marcarBoton(vista.getTablero().getE9(), apuntador);
			tablero.getTablero()[4][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}		
		
		if(ae.getActionCommand().equalsIgnoreCase("F1")){
			funciones.marcarBoton(vista.getTablero().getF1(), apuntador);
			tablero.getTablero()[5][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F2")){
			funciones.marcarBoton(vista.getTablero().getF2(), apuntador);
			tablero.getTablero()[5][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F3")){
			funciones.marcarBoton(vista.getTablero().getF3(), apuntador);
			tablero.getTablero()[5][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F4")){
			funciones.marcarBoton(vista.getTablero().getF4(), apuntador);
			tablero.getTablero()[5][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F5")){
			funciones.marcarBoton(vista.getTablero().getF5(), apuntador);
			tablero.getTablero()[5][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F6")){
			funciones.marcarBoton(vista.getTablero().getF6(), apuntador);
			tablero.getTablero()[5][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F7")){
			funciones.marcarBoton(vista.getTablero().getF7(), apuntador);
			tablero.getTablero()[5][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F8")){
			funciones.marcarBoton(vista.getTablero().getF8(), apuntador);
			tablero.getTablero()[5][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("F9")){
			funciones.marcarBoton(vista.getTablero().getF9(), apuntador);
			tablero.getTablero()[5][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}		
		
		if(ae.getActionCommand().equalsIgnoreCase("G1")){
			funciones.marcarBoton(vista.getTablero().getG1(), apuntador);
			tablero.getTablero()[6][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("G2")){
			funciones.marcarBoton(vista.getTablero().getG2(), apuntador);
			tablero.getTablero()[6][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("G3")){
			funciones.marcarBoton(vista.getTablero().getG3(), apuntador);
			tablero.getTablero()[6][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("G4")){
			funciones.marcarBoton(vista.getTablero().getG4(), apuntador);
			tablero.getTablero()[6][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		if(ae.getActionCommand()
				.equalsIgnoreCase("G5")){
			funciones.marcarBoton(vista.getTablero().getG5(), apuntador);
			tablero.getTablero()[6][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("G6")){
			funciones.marcarBoton(vista.getTablero().getG6(), apuntador);
			tablero.getTablero()[6][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("G7")){
			funciones.marcarBoton(vista.getTablero().getG7(), apuntador);
			tablero.getTablero()[6][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("G8")){
			funciones.marcarBoton(vista.getTablero().getG8(), apuntador);
			tablero.getTablero()[6][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("G9")){
			funciones.marcarBoton(vista.getTablero().getG9(), apuntador);
			tablero.getTablero()[6][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}		
		
		if(ae.getActionCommand().equalsIgnoreCase("H1")){
			funciones.marcarBoton(vista.getTablero().getH1(), apuntador);
			tablero.getTablero()[7][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H2")){
			funciones.marcarBoton(vista.getTablero().getH2(), apuntador);
			tablero.getTablero()[7][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H3")){
			funciones.marcarBoton(vista.getTablero().getH3(), apuntador);
			tablero.getTablero()[7][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H4")){
			funciones.marcarBoton(vista.getTablero().getH4(), apuntador);
			tablero.getTablero()[7][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H5")){
			funciones.marcarBoton(vista.getTablero().getH5(), apuntador);
			tablero.getTablero()[7][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H6")){
			funciones.marcarBoton(vista.getTablero().getH6(), apuntador);
			tablero.getTablero()[7][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H7")){
			funciones.marcarBoton(vista.getTablero().getH7(), apuntador);
			tablero.getTablero()[7][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H8")){
			funciones.marcarBoton(vista.getTablero().getH8(), apuntador);
			tablero.getTablero()[7][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("H9")){
			funciones.marcarBoton(vista.getTablero().getH9(), apuntador);
			tablero.getTablero()[7][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}		
		
		if(ae.getActionCommand().equalsIgnoreCase("I1")){
			funciones.marcarBoton(vista.getTablero().getI1(), apuntador);
			tablero.getTablero()[8][0] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I2")){
			funciones.marcarBoton(vista.getTablero().getI2(), apuntador);
			tablero.getTablero()[8][1] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I3")){
			funciones.marcarBoton(vista.getTablero().getI3(), apuntador);
			tablero.getTablero()[8][2] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I4")){
			funciones.marcarBoton(vista.getTablero().getI4(), apuntador);
			tablero.getTablero()[8][3] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I5")){
			funciones.marcarBoton(vista.getTablero().getI5(), apuntador);
			tablero.getTablero()[8][4] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I6")){
			funciones.marcarBoton(vista.getTablero().getI6(), apuntador);
			tablero.getTablero()[8][5] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I7")){
			funciones.marcarBoton(vista.getTablero().getI7(), apuntador);
			tablero.getTablero()[8][6] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I8")){
			funciones.marcarBoton(vista.getTablero().getI8(), apuntador);
			tablero.getTablero()[8][7] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
		
		if(ae.getActionCommand().equalsIgnoreCase("I9")){
			funciones.marcarBoton(vista.getTablero().getI9(), apuntador);
			tablero.getTablero()[8][8] = apuntador;
			if(funciones.isTableroLleno(tablero.getTablero()))vista.getHerramientas().getRevisar().setEnabled(true);
			else vista.getHerramientas().getRevisar().setEnabled(false);
		}
	}

	private void controlPausa(boolean estado){
		vista.getHerramientas().getB1().setEnabled(estado);
		vista.getHerramientas().getB2().setEnabled(estado);
		vista.getHerramientas().getB3().setEnabled(estado);
		vista.getHerramientas().getB4().setEnabled(estado);
		vista.getHerramientas().getB5().setEnabled(estado);
		vista.getHerramientas().getB6().setEnabled(estado);
		vista.getHerramientas().getB7().setEnabled(estado);
		vista.getHerramientas().getB8().setEnabled(estado);
		vista.getHerramientas().getB9().setEnabled(estado);
		vista.getHerramientas().getDificultadFacil().setEnabled(estado);
		vista.getHerramientas().getDificultadMedio().setEnabled(estado);
		vista.getHerramientas().getDificultadDificil().setEnabled(estado);
		vista.getHerramientas().getDificultadMuyDificil().setEnabled(estado);
		vista.getHerramientas().getNuevo().setEnabled(estado);
		vista.getHerramientas().getReiniciar().setEnabled(estado);
		vista.pausar((estado)?false:true);
	}
}
