package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import modelo.ModeloEstadisticas;
import modelo.ModeloJuego;
import vista.Estadisticas;

public class ControladorEstadisticas extends AbstractTableModel implements ActionListener{

	private static final long serialVersionUID = 1L;
	private Estadisticas vista = null;
	private ModeloEstadisticas estadisticas;
	private String[] nombresColumnas = null;
	private String[][] contenidoColumnas = null;
	
	public ControladorEstadisticas(Estadisticas vista){
		this.vista = vista;
		this.estadisticas = new ModeloEstadisticas();
		this.nombresColumnas = new String[4];
		this.nombresColumnas[0] = "No.";
		this.nombresColumnas[1] = "Nombre";
		this.nombresColumnas[2] = "Tiempo";
		this.nombresColumnas[3] = "Fecha";
		this.vista.getFacil().setSelected(true);
		this.setContenidoColumnas(ModeloEstadisticas.FACIL);
	}

	public void actionPerformed(ActionEvent ae) {
		if(ae.getActionCommand().equalsIgnoreCase("Facil")){
			vista.getFacil().setSelected(true);
			vista.getMedio().setSelected(false);
			vista.getDificil().setSelected(false);
			vista.getMuyDificil().setSelected(false);
			vista.actualizarTabla();
			this.setContenidoColumnas(ModeloEstadisticas.FACIL);			
		}

		if(ae.getActionCommand().equalsIgnoreCase("Medio")){
			vista.getFacil().setSelected(false);
			vista.getMedio().setSelected(true);
			vista.getDificil().setSelected(false);
			vista.getMuyDificil().setSelected(false);
			vista.actualizarTabla();
			this.setContenidoColumnas(ModeloEstadisticas.MEDIO);
		}

		if(ae.getActionCommand().equalsIgnoreCase("Dificil")){
			vista.getFacil().setSelected(false);
			vista.getMedio().setSelected(false);
			vista.getDificil().setSelected(true);
			vista.getMuyDificil().setSelected(false);
			vista.actualizarTabla();
			this.setContenidoColumnas(ModeloEstadisticas.DIFICIL);
		}

		if(ae.getActionCommand().equalsIgnoreCase("Muy Dificil")){
			vista.getFacil().setSelected(false);
			vista.getMedio().setSelected(false);
			vista.getDificil().setSelected(false);
			vista.getMuyDificil().setSelected(true);
			vista.actualizarTabla();
			this.setContenidoColumnas(ModeloEstadisticas.MUYDIFICIL);
		}
	}
	

	public int getColumnCount() {
		return nombresColumnas.length;
	}

	public int getRowCount() {
		return contenidoColumnas.length;
	}

	public Object getValueAt(int arg0, int arg1) {
		return contenidoColumnas[arg0][arg1];
	}

	public String getColumnName(int arg0) {
		return nombresColumnas[arg0];
	}
	
	private void setAnchoColumna(JTable table, int[] anchos){
		JViewport scroll = (JViewport) table.getParent();
		int ancho = scroll.getWidth();
		int anchoColumna;
		TableColumnModel columnModel = table.getColumnModel();
		TableColumn column;
		for(int i = 0;i<anchos.length;i++){
			column = columnModel.getColumn(i);
			anchoColumna = (anchos[i]*ancho)/100; 
			column.setPreferredWidth(anchoColumna);
		}
	}
	
	private void setContenidoColumnas(int dificultad){
		this.contenidoColumnas = new String[this.estadisticas.get(dificultad).size()][nombresColumnas.length];		
		ArrayList<ModeloJuego> juegos = estadisticas.get(dificultad);
		for(int i = 0;i<juegos.size();i++){
			contenidoColumnas[i][0] = String.valueOf(i+1);
			contenidoColumnas[i][1] = juegos.get(i).getNombre();
			contenidoColumnas[i][2] = calularTiempo(juegos.get(i).getPuntuacion());
			contenidoColumnas[i][3] = juegos.get(i).getFecha();
		}
		this.vista.getTablaEstadisticas().setModel(this);
		this.vista.getTablaEstadisticas().setShowGrid(false);
		int[] anchos = {7,45,13,35};
		this.setAnchoColumna(this.vista.getTablaEstadisticas(), anchos);
	}
	
	private String calularTiempo(int puntuacion){
		int horas = (puntuacion > 3600)?puntuacion/3600:0;
		int restante = (puntuacion > 3600)?puntuacion%3600:puntuacion;
		int minutos = restante/60;
		int segundos =  restante%60;
		String tiempo = "";
		if(horas > 0) tiempo = horas+":";
		tiempo = (minutos < 9)?tiempo+"0"+minutos+":":tiempo+minutos+":";
		tiempo = (segundos < 9)?tiempo+"0"+segundos:tiempo+segundos;
		return tiempo;
	}
}
