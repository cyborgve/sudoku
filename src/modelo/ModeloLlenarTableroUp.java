package modelo;

import java.awt.Point;

import vista.Principal;

public class ModeloLlenarTableroUp implements Runnable {
	
	private Principal vista = null;
	private int[][] tablero = null;
	private ModeloFunciones funciones = null;
	private Thread hilo = null;
	
	public ModeloLlenarTableroUp(Principal vista,int[][] tablero){
		this.vista = vista;
		this.tablero = tablero;
		this.funciones = new ModeloFunciones();
		this.hilo = new Thread(this);
		this.hilo.start();
	}
	
	private void marcarCasillaNegra(Point point){
		if((point.x == 0) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getA1(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getB1(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getC1(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getD1(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getE1(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getF1(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getG1(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getH1(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 0))funciones.marcarBotonAnimado(vista.getTablero().getI1(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getA2(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getB2(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getC2(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getD2(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getE2(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getF2(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getG2(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getH2(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 1))funciones.marcarBotonAnimado(vista.getTablero().getI2(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getA3(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getB3(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getC3(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getD3(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getE3(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getF3(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getG3(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getH3(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 2))funciones.marcarBotonAnimado(vista.getTablero().getI3(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getA4(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getB4(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getC4(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getD4(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getE4(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getF4(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getG4(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getH4(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 3))funciones.marcarBotonAnimado(vista.getTablero().getI4(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getA5(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getB5(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getC5(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getD5(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getE5(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getF5(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getG5(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getH5(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 4))funciones.marcarBotonAnimado(vista.getTablero().getI5(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getA6(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getB6(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getC6(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getD6(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getE6(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getF6(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getG6(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getH6(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 5))funciones.marcarBotonAnimado(vista.getTablero().getI6(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getA7(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getB7(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getC7(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getD7(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getE7(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getF7(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getG7(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getH7(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 6))funciones.marcarBotonAnimado(vista.getTablero().getI7(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getA8(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getB8(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getC8(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getD8(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getE8(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getF8(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getG8(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getH8(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 7))funciones.marcarBotonAnimado(vista.getTablero().getI8(),tablero[point.x][point.y]);
		if((point.x == 0) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getA9(),tablero[point.x][point.y]);
		if((point.x == 1) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getB9(),tablero[point.x][point.y]);
		if((point.x == 2) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getC9(),tablero[point.x][point.y]);
		if((point.x == 3) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getD9(),tablero[point.x][point.y]);
		if((point.x == 4) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getE9(),tablero[point.x][point.y]);
		if((point.x == 5) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getF9(),tablero[point.x][point.y]);
		if((point.x == 6) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getG9(),tablero[point.x][point.y]);
		if((point.x == 7) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getH9(),tablero[point.x][point.y]);
		if((point.x == 8) && (point.y == 8))funciones.marcarBotonAnimado(vista.getTablero().getI9(),tablero[point.x][point.y]);
	}

	public synchronized void run() {
		try {
			for(int y = 8;y > -1;y--){
				for(int x = 8;x > -1;x--){
					marcarCasillaNegra(new Point(x,y));
					Thread.sleep(12);
				}
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}		
	}
}
