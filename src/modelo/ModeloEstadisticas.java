package modelo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;



public class ModeloEstadisticas implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<ModeloJuego> facil = null;
	private ArrayList<ModeloJuego> medio = null;
	private ArrayList<ModeloJuego> dificil = null;
	private ArrayList<ModeloJuego> muyDificil = null;
	public static final int FACIL = 1;
	public static final int MEDIO = 2;
	public static final int DIFICIL = 3;
	public static final int MUYDIFICIL = 4;
	private File file = null;
	
	public ModeloEstadisticas(){
		this.file = new File(System.getenv("USERPROFILE")+"/sudoku.dat");
		if(file.exists()){
			this.facil = cargar().get(ModeloEstadisticas.FACIL);		
			this.medio = cargar().get(ModeloEstadisticas.MEDIO);		
			this.dificil = cargar().get(ModeloEstadisticas.DIFICIL);		
			this.muyDificil = cargar().get(ModeloEstadisticas.MUYDIFICIL);
		}else{
			crearValoresIniciales();
			this.facil = cargar().get(ModeloEstadisticas.FACIL);		
			this.medio = cargar().get(ModeloEstadisticas.MEDIO);		
			this.dificil = cargar().get(ModeloEstadisticas.DIFICIL);		
			this.muyDificil = cargar().get(ModeloEstadisticas.MUYDIFICIL);
		}
		
	}

	private ArrayList<ModeloJuego> getDificil() {
		if(dificil == null){
			dificil = new ArrayList<ModeloJuego>();
		}return dificil;
	}

	private ArrayList<ModeloJuego> getFacil() {
		if(facil == null){
			facil = new ArrayList<ModeloJuego>();
		}return facil;
	}

	private ArrayList<ModeloJuego> getMedio() {
		if(medio == null){
			medio = new ArrayList<ModeloJuego>();
		}return medio;
	}

	private ArrayList<ModeloJuego> getMuyDificil() {
		if(muyDificil == null){
			muyDificil = new ArrayList<ModeloJuego>();
		}return muyDificil;
	}
	
	public ArrayList<ModeloJuego> get(int dificultad){
		if(dificultad == 1) return getFacil();
		if(dificultad == 2) return getMedio();
		if(dificultad == 3) return getDificil();
		if(dificultad == 4) return getMuyDificil();
		return null;
	}
	
	public boolean nuevoRecord(ModeloJuego juego, int dificultad){
		for(int i = 0;i<this.get(dificultad).size();i++){
			if(juego.getPuntuacion() < this.get(dificultad).get(i).getPuntuacion())
				return true;
		}
		return false;
	}
	
	public void set(ModeloJuego juego, int dificultad){
		ArrayList<ModeloJuego> temp = this.get(dificultad);
		ArrayList<ModeloJuego> nuevo = new ArrayList<ModeloJuego>();
		boolean intertado = false;
		for(int i = 0;i<10;i++){
			for(int j = 0;j<temp.size();j++){
				if((juego.getPuntuacion()<temp.get(j).getPuntuacion()) && (intertado == false)){
					nuevo.add(juego);
					temp.remove(j);
					intertado = true;
					j = temp.size()+1;
				}else{
					nuevo.add(temp.get(j));
					temp.remove(j);
					j = temp.size()+1;
				}
			}
		}this.get(dificultad).clear();
		this.get(dificultad).addAll(nuevo);
		guardar();
	}
	
	private void guardar(){
		try {
			ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file.getAbsolutePath()));
			outputStream.writeObject(this);
			outputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private ModeloEstadisticas cargar(){
		ModeloEstadisticas estadisticas = null;
		try {
			ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file.getAbsolutePath()));
			estadisticas = (ModeloEstadisticas) inputStream.readObject();
			inputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}return estadisticas;
	}
	
	private void crearValoresIniciales(){
		getFacil().addAll(new ModeloEstadisticaInicial().getFacil());
		getMedio().addAll(new ModeloEstadisticaInicial().getMedio());
		getDificil().addAll(new ModeloEstadisticaInicial().getDificil());
		getMuyDificil().addAll(new ModeloEstadisticaInicial().getMuyDificil());
		guardar();
	}
}
