package modelo;

import java.io.Serializable;

public class ModeloJuego implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nombre = null;
	private String fecha = null;
	private int puntuacion = 0;
	
	public String getFecha() {
		if(fecha == null){
			fecha = "";
		}
		return fecha;
	}
	
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public String getNombre() {
		if(nombre == null){
			nombre = "";
		}
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getPuntuacion() {
		return puntuacion;
	}
	
	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}
	
	public String toString(){
		return getNombre()+" "+getPuntuacion()+" "+getFecha();
	}
}
