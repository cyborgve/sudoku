package modelo;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class ModeloFunciones {
		
	public void marcarBoton(JButton button,int apuntador){
		if(apuntador != 0) button.setIcon(new ImageIcon(getClass().getResource(elegirImagenNegra(apuntador))));
		else button.setIcon(null);
	}
	
	public void fijarBoton(JButton button, int apuntador){
		if(apuntador != 0){
			button.setIcon(new ImageIcon(getClass().getResource(elegirImagenAzul(apuntador))));
			button.setActionCommand(null);
			button.setFocusable(false);
		}		
	}
	
	public void marcarBotonAnimado(JButton button,int apuntador){
		button.setIcon(new ImageIcon(getClass().getResource("/imagenes/negro.jpg")));
	}
	
	public boolean isTableroLleno(int[][] tablero){
		for(int y = 0; y < 9; y++){
			for(int x = 0; x < 9; x++)
				if(tablero[x][y]==0)return false;
		}		
		return true;
	}
	
	private String elegirImagenNegra(int arg0){
		String imagen = null;
		switch (arg0) {
			case 1:
				imagen = "/imagenes/1.png";
				break;
			case 2:
				imagen = "/imagenes/2.png";
				break;
			case 3:
				imagen = "/imagenes/3.png";
				break;
			case 4:
				imagen = "/imagenes/4.png";
				break;
			case 5:
				imagen = "/imagenes/5.png";
				break;
			case 6:
				imagen = "/imagenes/6.png";
				break;
			case 7:
				imagen = "/imagenes/7.png";
				break;
			case 8:
				imagen = "/imagenes/8.png";
				break;
			case 9:
				imagen = "/imagenes/9.png";
				break;				
		}return imagen;
	}
	
	private String elegirImagenAzul(int arg0){
		String imagen = null;
		switch (arg0) {
			case 1:
				imagen = "/imagenes/1a.png";
				break;
			case 2:
				imagen = "/imagenes/2a.png";
				break;
			case 3:
				imagen = "/imagenes/3a.png";
				break;
			case 4:
				imagen = "/imagenes/4a.png";
				break;
			case 5:
				imagen = "/imagenes/5a.png";
				break;
			case 6:
				imagen = "/imagenes/6a.png";
				break;
			case 7:
				imagen = "/imagenes/7a.png";
				break;
			case 8:
				imagen = "/imagenes/8a.png";
				break;
			case 9:
				imagen = "/imagenes/9a.png";
				break;				
		}return imagen;
	}
	
	private ModeloTablero llenarVectores(ModeloTablero tablero){
		for(int x = 0;x<9;x++){
			for(int y = 0;y<9;y++){			
				if(x == 0) tablero.getA()[y] = tablero.getTablero()[x][y];
				if(x == 1) tablero.getB()[y] = tablero.getTablero()[x][y];
				if(x == 2) tablero.getC()[y] = tablero.getTablero()[x][y];
				if(x == 3) tablero.getD()[y] = tablero.getTablero()[x][y];
				if(x == 4) tablero.getE()[y] = tablero.getTablero()[x][y];
				if(x == 5) tablero.getF()[y] = tablero.getTablero()[x][y];
				if(x == 6) tablero.getG()[y] = tablero.getTablero()[x][y];
				if(x == 7) tablero.getH()[y] = tablero.getTablero()[x][y];
				if(x == 8) tablero.getI()[y] = tablero.getTablero()[x][y];				
				if(y == 0) tablero.getUNO()[x] = tablero.getTablero()[x][y];
				if(y == 1) tablero.getDOS()[x] = tablero.getTablero()[x][y];
				if(y == 2) tablero.getTRES()[x] = tablero.getTablero()[x][y];
				if(y == 3) tablero.getCUATRO()[x] = tablero.getTablero()[x][y];
				if(y == 4) tablero.getCINCO()[x] = tablero.getTablero()[x][y];
				if(y == 5) tablero.getSEIS()[x] = tablero.getTablero()[x][y];
				if(y == 6) tablero.getSIETE()[x] = tablero.getTablero()[x][y];
				if(y == 7) tablero.getOCHO()[x] = tablero.getTablero()[x][y];
				if(y == 8) tablero.getNUEVE()[x] = tablero.getTablero()[x][y];				
			}
		}
		return tablero;
	}
	
	public boolean revisarJugada(ModeloTablero tablero){
		llenarVectores(tablero);
		if(vectorCorrecto(tablero.getA()) == false) return false;
		if(vectorCorrecto(tablero.getB()) == false) return false;
		if(vectorCorrecto(tablero.getC()) == false) return false;
		if(vectorCorrecto(tablero.getD()) == false) return false;
		if(vectorCorrecto(tablero.getE()) == false) return false;
		if(vectorCorrecto(tablero.getF()) == false) return false;
		if(vectorCorrecto(tablero.getG()) == false) return false;
		if(vectorCorrecto(tablero.getH()) == false) return false;
		if(vectorCorrecto(tablero.getI()) == false) return false;
		if(vectorCorrecto(tablero.getUNO()) == false) return false;
		if(vectorCorrecto(tablero.getDOS()) == false) return false;
		if(vectorCorrecto(tablero.getTRES()) == false) return false;
		if(vectorCorrecto(tablero.getCUATRO()) == false) return false;
		if(vectorCorrecto(tablero.getCINCO()) == false) return false;
		if(vectorCorrecto(tablero.getSEIS()) == false) return false;
		if(vectorCorrecto(tablero.getSIETE()) == false) return false;
		if(vectorCorrecto(tablero.getOCHO()) == false) return false;
		if(vectorCorrecto(tablero.getNUEVE()) == false) return false;
		return true;
	}
	
	private boolean vectorCorrecto(int[] vector){
		int posicionActual = 0;
		int[] registros = new int[vector.length];
		for(int x = 0;x < vector.length;x++){
			if(posicionActual == 0) registros[posicionActual++] = vector[x];
			else{
				for(int i = 0;i < posicionActual;i++){
					if(registros[i] == vector[x]) return false;
					else registros[posicionActual] = vector[x];
				}
				posicionActual++;
			}
		}
		return true;
	}
	
	public int[] generarPartida(int longitud){
		int[] resultado = new int[longitud];
		for(int i = 0;i<longitud;){
			int aleatorio = (int)(Math.random()*81+1);
			if(i == 0) resultado[i++] = aleatorio;
			else{
				boolean continuar = true;
				for(int j = 0;j<=i && continuar;j++){
					if(resultado[j] == aleatorio) continuar = false;
					if((j == i) && (resultado[j] != aleatorio)){
						resultado[i++] = aleatorio;
						continuar = false;
					}
				}
			}
		}
		return resultado;
	}
	
	public int[][] marcarJugadas(int[][] tablero,int[] jugadas){
		for(int i = 0;i<jugadas.length;i++){
			if(jugadas[i] == 1) tablero[0][0] = 0;
			if(jugadas[i] == 2) tablero[1][0] = 0;
			if(jugadas[i] == 3) tablero[2][0] = 0;
			if(jugadas[i] == 4) tablero[3][0] = 0;
			if(jugadas[i] == 5) tablero[4][0] = 0;
			if(jugadas[i] == 6) tablero[5][0] = 0;
			if(jugadas[i] == 7) tablero[6][0] = 0;
			if(jugadas[i] == 8) tablero[7][0] = 0;
			if(jugadas[i] == 9) tablero[8][0] = 0;
			if(jugadas[i] == 10) tablero[0][1] = 0;
			if(jugadas[i] == 11) tablero[1][1] = 0;
			if(jugadas[i] == 12) tablero[2][1] = 0;
			if(jugadas[i] == 13) tablero[3][1] = 0;
			if(jugadas[i] == 14) tablero[4][1] = 0;
			if(jugadas[i] == 15) tablero[5][1] = 0;
			if(jugadas[i] == 16) tablero[6][1] = 0;
			if(jugadas[i] == 17) tablero[7][1] = 0;
			if(jugadas[i] == 18) tablero[8][1] = 0;
			if(jugadas[i] == 19) tablero[0][2] = 0;
			if(jugadas[i] == 20) tablero[1][2] = 0;
			if(jugadas[i] == 21) tablero[2][2] = 0;
			if(jugadas[i] == 22) tablero[3][2] = 0;
			if(jugadas[i] == 23) tablero[4][2] = 0;
			if(jugadas[i] == 24) tablero[5][2] = 0;
			if(jugadas[i] == 25) tablero[6][2] = 0;
			if(jugadas[i] == 26) tablero[7][2] = 0;
			if(jugadas[i] == 27) tablero[8][2] = 0;
			if(jugadas[i] == 28) tablero[0][3] = 0;
			if(jugadas[i] == 29) tablero[1][3] = 0;
			if(jugadas[i] == 30) tablero[2][3] = 0;
			if(jugadas[i] == 31) tablero[3][3] = 0;
			if(jugadas[i] == 32) tablero[4][3] = 0;
			if(jugadas[i] == 33) tablero[5][3] = 0;
			if(jugadas[i] == 34) tablero[6][3] = 0;
			if(jugadas[i] == 35) tablero[7][3] = 0;
			if(jugadas[i] == 36) tablero[8][3] = 0;
			if(jugadas[i] == 37) tablero[0][4] = 0;
			if(jugadas[i] == 38) tablero[1][4] = 0;
			if(jugadas[i] == 39) tablero[2][4] = 0;
			if(jugadas[i] == 40) tablero[3][4] = 0;
			if(jugadas[i] == 41) tablero[4][4] = 0;
			if(jugadas[i] == 42) tablero[5][4] = 0;
			if(jugadas[i] == 43) tablero[6][4] = 0;
			if(jugadas[i] == 44) tablero[7][4] = 0;
			if(jugadas[i] == 45) tablero[8][4] = 0;
			if(jugadas[i] == 46) tablero[0][5] = 0;
			if(jugadas[i] == 47) tablero[1][5] = 0;
			if(jugadas[i] == 48) tablero[2][5] = 0;
			if(jugadas[i] == 49) tablero[3][5] = 0;
			if(jugadas[i] == 50) tablero[4][5] = 0;
			if(jugadas[i] == 51) tablero[5][5] = 0;
			if(jugadas[i] == 52) tablero[6][5] = 0;
			if(jugadas[i] == 53) tablero[7][5] = 0;
			if(jugadas[i] == 54) tablero[8][5] = 0;
			if(jugadas[i] == 55) tablero[0][6] = 0;
			if(jugadas[i] == 56) tablero[1][6] = 0;
			if(jugadas[i] == 57) tablero[2][6] = 0;
			if(jugadas[i] == 58) tablero[3][6] = 0;
			if(jugadas[i] == 59) tablero[4][6] = 0;
			if(jugadas[i] == 60) tablero[5][6] = 0;
			if(jugadas[i] == 61) tablero[6][6] = 0;
			if(jugadas[i] == 62) tablero[7][6] = 0;
			if(jugadas[i] == 63) tablero[8][6] = 0;
			if(jugadas[i] == 64) tablero[0][7] = 0;
			if(jugadas[i] == 65) tablero[1][7] = 0;
			if(jugadas[i] == 66) tablero[2][7] = 0;
			if(jugadas[i] == 67) tablero[3][7] = 0;
			if(jugadas[i] == 68) tablero[4][7] = 0;
			if(jugadas[i] == 69) tablero[5][7] = 0;
			if(jugadas[i] == 70) tablero[6][7] = 0;
			if(jugadas[i] == 71) tablero[7][7] = 0;
			if(jugadas[i] == 72) tablero[8][7] = 0;
			if(jugadas[i] == 73) tablero[0][8] = 0;
			if(jugadas[i] == 74) tablero[1][8] = 0;
			if(jugadas[i] == 75) tablero[2][8] = 0;
			if(jugadas[i] == 76) tablero[3][8] = 0;
			if(jugadas[i] == 77) tablero[4][8] = 0;
			if(jugadas[i] == 78) tablero[5][8] = 0;
			if(jugadas[i] == 79) tablero[6][8] = 0;
			if(jugadas[i] == 80) tablero[7][8] = 0;
			if(jugadas[i] == 81) tablero[8][8] = 0;
		}
		return tablero;
	}
	
	public int[][] crearPartida(int dificultad){
		int[][] partidaLlena = new ModeloSudoku().getTablero();
		int[] puestosCero = generarPartida(dificultad);
		int[][] partidaNueva = marcarJugadas(partidaLlena,puestosCero);
		return partidaNueva;
	}
}
