package modelo;

public class ModeloTablero {

	private int[] UNO = null, DOS = null, TRES = null, CUATRO = null, CINCO = null, SEIS = null, SIETE = null, OCHO = null, NUEVE = null;
	private int[] A = null, B = null, C = null, D = null, E = null, F = null, G = null, H = null, I = null;
	private int[][] tablero = null;
	
	public int[] getUNO() {
		if(UNO == null){
			UNO = new int[9];
		}return UNO;
	}

	public int[] getDOS() {
		if(DOS == null){
			DOS = new int[9];
		}return DOS;
	}

	public int[] getTRES() {
		if(TRES == null){
			TRES = new int[9];
		}return TRES;
	}

	public int[] getCUATRO() {
		if(CUATRO == null){
			CUATRO = new int[9];
		}return CUATRO;
	}

	public int[] getCINCO() {
		if(CINCO == null){
			CINCO = new int[9];
		}return CINCO;
	}

	public int[] getSEIS() {
		if(SEIS == null){
			SEIS = new int[9];
		}return SEIS;
	}

	public int[] getSIETE() {
		if(SIETE == null){
			SIETE = new int[9];
		}return SIETE;
	}

	public int[] getOCHO() {
		if(OCHO == null){
			OCHO = new int[9];
		}return OCHO;
	}

	public int[] getNUEVE() {
		if(NUEVE == null){
			NUEVE = new int[9];
		}return NUEVE;
	}

	public int[] getA() {
		if(A == null){
			A = new int[9];
		}return A;
	}

	public int[] getB() {
		if(B == null){
			B = new int[9];
		}return B;
	}

	public int[] getC() {
		if(C == null){
			C = new int[9];
		}return C;
	}

	public int[] getD() {
		if(D == null){
			D = new int[9];
		}return D;
	}

	public int[] getE() {
		if(E == null){
			E = new int[9];
		}return E;
	}

	public int[] getF() {
		if(F == null){
			F = new int[9];
		}return F;
	}

	public int[] getG() {
		if(G == null){
			G = new int[9];
		}return G;
	}

	public int[] getH() {
		if(H == null){
			H = new int[9];
		}return H;
	}

	public int[] getI() {
		if(I == null){
			I = new int[9];
		}return I;
	}

	public int[][] getTablero() {
		if(tablero == null){
			tablero = new int[9][9];
		}return tablero;
	}
	
	public void setTablero(int[][] tablero){
		this.tablero = tablero;
	}
}
