package modelo;

import java.util.Calendar;

public class ModeloFecha {
	
	private String dia;
	private String mes;
	private String anio;
	private String hora;
	private String minuto;
	private String ampm;
	
	public ModeloFecha(){
		Calendar calendar = Calendar.getInstance();
		dia = (calendar.get(Calendar.DAY_OF_MONTH)<9)?"0"+calendar.get(Calendar.DAY_OF_MONTH):calendar.get(Calendar.DAY_OF_MONTH)+"";
		int month = (calendar.get(Calendar.MONTH))+1;
		mes = (month<9)?"0"+month:month+"";
		anio = calendar.get(Calendar.YEAR)+"";
		hora = (calendar.get(Calendar.HOUR_OF_DAY)<9)?"0"+calendar.get(Calendar.HOUR):calendar.get(Calendar.HOUR)+"";
		minuto = (calendar.get(Calendar.MINUTE)<9)?"0"+calendar.get(Calendar.MINUTE):calendar.get(Calendar.MINUTE)+"";
		ampm = (calendar.get(Calendar.AM_PM) == 0)?"am":"pm";
	}
	
	public String toString(){
		return dia+"/"+mes+"/"+anio+" "+hora+":"+minuto+ampm;
	}
}
