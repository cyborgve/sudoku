package modelo;

import java.awt.Point;

import vista.Cronometro;
import vista.Principal;

public class ModeloLlenarTableroDown implements Runnable {
	
	private Principal vista = null;
	private int[][] tablero = null;
	private ModeloFunciones funciones = null;
	private Thread hilo = null;
	
	public ModeloLlenarTableroDown(Principal vista,int[][] tablero){
		this.vista = vista;
		this.tablero = tablero;
		this.funciones = new ModeloFunciones();
		this.hilo = new Thread(this);
		this.hilo.start();
	}
	
	private synchronized void marcarCasillaValida(Point point){
		if((point.x == 0) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA1(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB1(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC1(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD1(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE1(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF1(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG1(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH1(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 0)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI1(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI1(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA2(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB2(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC2(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD2(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE2(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF2(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG2(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH2(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 1)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI2(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI2(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA3(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB3(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC3(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD3(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE3(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF3(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG3(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH3(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 2)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI3(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI3(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA4(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB4(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC4(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD4(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE4(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF4(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG4(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH4(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 3)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI4(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI4(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA5(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB5(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC5(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD5(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE5(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF5(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG5(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH5(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 4)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI5(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI5(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA6(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB6(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC6(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD6(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE6(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF6(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG6(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH6(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 5)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI6(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI6(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA7(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB7(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC7(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD7(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE7(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF7(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG7(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH7(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 6)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI7(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI7(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA8(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB8(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC8(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD8(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE8(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF8(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG8(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH8(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 7)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI8(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI8(),tablero[point.x][point.y]);}
		if((point.x == 0) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getA9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getA9(),tablero[point.x][point.y]);}
		if((point.x == 1) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getB9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getB9(),tablero[point.x][point.y]);}
		if((point.x == 2) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getC9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getC9(),tablero[point.x][point.y]);}
		if((point.x == 3) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getD9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getD9(),tablero[point.x][point.y]);}
		if((point.x == 4) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getE9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getE9(),tablero[point.x][point.y]);}
		if((point.x == 5) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getF9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getF9(),tablero[point.x][point.y]);}
		if((point.x == 6) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getG9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getG9(),tablero[point.x][point.y]);}
		if((point.x == 7) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getH9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getH9(),tablero[point.x][point.y]);}
		if((point.x == 8) && (point.y == 8)){ if(tablero[point.x][point.y] == 0)funciones.marcarBoton(vista.getTablero().getI9(),tablero[point.x][point.y]);else funciones.fijarBoton(vista.getTablero().getI9(),tablero[point.x][point.y]);}
	}
	
	public void run() {
		try {
			Thread.sleep(1500);
			for(int y = 0;y < 9;y++){
				for(int x = 0;x < 9;x++){
					marcarCasillaValida(new Point(x,y));
					Thread.sleep(12);
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		vista.getHerramientas().getNuevo().setEnabled(true);
		vista.getHerramientas().getReiniciar().setEnabled(true);
		vista.getHerramientas().getPausa().setEnabled(true);
		vista.getHerramientas().getCronometro().control(Cronometro.INICIAR);
		vista.getHerramientas().getPausa().setEnabled(true);
	}
}
