package modelo;

import java.io.Serializable;
import java.util.ArrayList;

public class ModeloEstadisticaInicial implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String[] nombre = {"Juan","Pedro","Jose","Ramon","Antonio","Maria","Carmen","Rosa","Ana","Raquel"};
	private String[] apellido = {"Alvarez","Guedez","Perez","Gonzalez","Ramirez","Lopez","Rodriguez","Delgadillo","Contreras","Martinez"};
	private ArrayList<ModeloJuego> facil = new ArrayList<ModeloJuego>();
	private ArrayList<ModeloJuego> medio = new ArrayList<ModeloJuego>();
	private ArrayList<ModeloJuego> dificil = new ArrayList<ModeloJuego>();
	private ArrayList<ModeloJuego> muyDificil = new ArrayList<ModeloJuego>();
	
	public ModeloEstadisticaInicial(){
		llenar(facil, 540);
		llenar(medio, 720);
		llenar(dificil, 960);
		llenar(muyDificil, 1320);		
	}
	
	private void llenar(ArrayList<ModeloJuego> juegos,int inicio){
		for(int i = 0;i<10;i++){
			ModeloJuego juego = new ModeloJuego();
			juego.setNombre((nombre[(int)(Math.random()*9+0)])+" "+(apellido[(int)(Math.random()*9+0)]));
			juego.setPuntuacion(inicio);
			juego.setFecha(new ModeloFecha().toString());
			juegos.add(juego);
			inicio +=15;
		}
	}

	public ArrayList<ModeloJuego> getDificil() {
		return dificil;
	}

	public ArrayList<ModeloJuego> getFacil() {
		return facil;
	}

	public ArrayList<ModeloJuego> getMedio() {
		return medio;
	}

	public ArrayList<ModeloJuego> getMuyDificil() {
		return muyDificil;
	}
}
