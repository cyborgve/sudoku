package modelo;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

public class ModeloSudoku {

	private int[][] tablero;
	private Random aleatorio;
	private ArrayList<Integer> listaEnteros;
	private int longitud;
	private int longitudZona;

	public ModeloSudoku() {
		longitudZona = 3;
		this.longitud = 9;
		aleatorio = new Random();
		listaEnteros = new ArrayList<Integer>();
		generate();
	}

	private boolean generarFila(int fila) {
		for (int columna = 0; columna < longitud; columna++) {
			if (llenarListaEnteros(fila, columna) == 0) {
				return false;
			}
			tablero[fila][columna] = listaEnteros.remove(aleatorio
					.nextInt(listaEnteros.size()));
		}
		return true;
	}

	public void generate() {
		boolean crear = true;
		int filaActual = 0;
		int[] vector = new int[longitud];
		tablero = new int[longitud][longitud];
		while (filaActual < longitud) {
			vector[filaActual]++;
			if (generarFila(filaActual)) {
				filaActual++;
				continue;
			}
			if (vector[filaActual] < longitudZona * longitudZona * longitudZona
					* 2) {
				continue;
			}
			if (crear)
			while (filaActual % longitudZona != 0) {
				vector[filaActual--] = 0;
			}
			vector[filaActual] = 0;
		}

		for (int i = 0; i < longitud; i++) {
			for (int j = 0; j < longitud; j++) {
				tablero[i][j]++;
			}
		}
	}

	private int llenarListaEnteros(int fila, int columna) {
		boolean[] disponible = new boolean[longitud];
		for (int i = 0; i < longitud; i++)
			disponible[i] = true;
		for (int i = 0; i < fila; i++)
			disponible[tablero[i][columna]] = false;
		for (int i = 0; i < columna; i++)
			disponible[tablero[fila][i]] = false;
		Point intervaloFila = zonaFilaColumna(fila);
		Point colRange = zonaFilaColumna(columna);
		for (int i = intervaloFila.x; i < fila; i++) {
			for (int j = colRange.x; j <= colRange.y; j++) {
				disponible[tablero[i][j]] = false;
			}
		}

		listaEnteros.clear();
		for (int i = 0; i < longitud; i++) {
			if (disponible[i])
				listaEnteros.add(i);
		}
		return listaEnteros.size();
	}

	private Point zonaFilaColumna(int filaColumna) {
		int x = (filaColumna / longitudZona) * longitudZona;
		int y = x + longitudZona - 1;
		Point point = new Point(x, y);
		return point;
	}

	public int[][] getTablero() {
		return tablero;
	}
}
