package vista;

import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Cronometro extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	private Thread tiempo = null;
	private int hora = 0, minuto = 0,segundo = 0;
	private JLabel momentoActual = null;
	private boolean cronometro;
	public static final int INICIAR = 1;
	public static final int DETENER = 2;
	public static final int RESTABLECER = 3;

	public Cronometro() {
		super();
		initialize();
	}

	private void initialize() {
		this.setLayout(new GridBagLayout());
		this.setVisible(true);
		this.add(getMomentoActual());
		this.setLayout(new GridLayout());
	}
	
	public JLabel getMomentoActual(){
		if(momentoActual==null){
			momentoActual = new JLabel();
			momentoActual.setText("00:00:00");
			momentoActual.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return momentoActual;
	}
	
	public int getResultado(){
		return ((hora*3600)+(minuto*60)+segundo);
	}
	
	public void run() {
		try {
			while(cronometro){
				segundo++;
					if(segundo == 60){
						segundo = 0;
						minuto++;
						if(minuto == 60){
							minuto = 0;
							hora++;
						}
					}
					
				getMomentoActual().setText(actualizarEstado(hora, minuto, segundo));
				Thread.sleep(1000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private String actualizarEstado(int h,int m,int s){
		String hora = String.valueOf(h);
		String minuto = String.valueOf(m);
		String segundo = String.valueOf(s);
		
		if(hora.length()<2) hora = "0"+hora;
		if(minuto.length()<2) minuto = "0"+minuto;
		if(segundo.length()<2) segundo = "0"+segundo;	
		return hora+":"+minuto+":"+segundo;
	}
	
	public void control(int accion){
		switch (accion) {
		case 1:
			this.cronometro = true;
			if(tiempo == null){
				tiempo = new Thread(this);
				tiempo.start();
			}
			break;
		case 2:
			this.cronometro = false;
			tiempo = null;
			break;
		case 3:
			this.hora = 0;
			this.minuto = 0;
			this.segundo = 0;
			getMomentoActual().setText(actualizarEstado(hora, minuto, segundo));
			break;
		}
	}
}
