package vista;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

public class Herramientas extends JPanel{

	private static final long serialVersionUID = 1L;
	private JPanel panelNorte = null;
	private JPanel panelCentral = null;
	private JPanel panelSur= null;
	private JToggleButton b1 = null;
	private JToggleButton b2 = null;
	private JToggleButton b3 = null;
	private JToggleButton b4 = null;
	private JToggleButton b5 = null;
	private JToggleButton b6 = null;
	private JToggleButton b7 = null;
	private JToggleButton b8 = null;
	private JToggleButton b9 = null;
	private JLabel difucultad = null;
	private JRadioButton dificultadFacil = null;
	private JRadioButton dificultadMedio = null;
	private JRadioButton dificultadDificil = null;
	private JRadioButton dificultadMuyDificil = null;
	private JLabel tiempo= null;
	private Cronometro cronometro = null;
	private JButton revisar = null;
	private JButton nuevo = null;
	private JButton reiniciar = null;
	private JButton pausa = null;
	
	public Herramientas() {
		super();
		initialize();
	}

	private void initialize() {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		this.setSize(new Dimension(160, 377));
		this.add(getPanelNorte(), BorderLayout.NORTH);
		this.add(getPanelCentral(),BorderLayout.CENTER);
		this.add(getPanelSur(), BorderLayout.SOUTH);
	}

	public JToggleButton getB1() {
		if(b1 == null){
			b1 = new JToggleButton();
			b1.setIcon(new ImageIcon(getClass().getResource("/imagenes/1.png")));
			b1.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b1.setActionCommand("1");
		}
		return b1;
	}

	public JToggleButton getB2() {
		if(b2 == null){
			b2 = new JToggleButton();
			b2.setIcon(new ImageIcon(getClass().getResource("/imagenes/2.png")));
			b2.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b2.setActionCommand("2");
		}
		return b2;
	}

	public JToggleButton getB3() {
		if(b3 == null){
			b3 = new JToggleButton();
			b3.setIcon(new ImageIcon(getClass().getResource("/imagenes/3.png")));
			b3.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b3.setActionCommand("3");
		}
		return b3;
	}

	public JToggleButton getB4() {
		if(b4 == null){
			b4 = new JToggleButton();
			b4.setIcon(new ImageIcon(getClass().getResource("/imagenes/4.png")));
			b4.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b4.setActionCommand("4");
		}
		return b4;
	}

	public JToggleButton getB5() {
		if(b5 == null){
			b5 = new JToggleButton();
			b5.setIcon(new ImageIcon(getClass().getResource("/imagenes/5.png")));
			b5.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b5.setActionCommand("5");
		}
		return b5;
	}

	public JToggleButton getB6() {
		if(b6 == null){
			b6 = new JToggleButton();
			b6.setIcon(new ImageIcon(getClass().getResource("/imagenes/6.png")));
			b6.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b6.setActionCommand("6");
		}
		return b6;
	}

	public JToggleButton getB7() {
		if(b7 == null){
			b7 = new JToggleButton();
			b7.setIcon(new ImageIcon(getClass().getResource("/imagenes/7.png")));
			b7.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b7.setActionCommand("7");
		}
		return b7;
	}

	public JToggleButton getB8() {
		if(b8 == null){
			b8 = new JToggleButton();
			b8.setIcon(new ImageIcon(getClass().getResource("/imagenes/8.png")));
			b8.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b8.setActionCommand("8");
		}
		return b8;
	}

	public JToggleButton getB9() {
		if(b9 == null){
			b9 = new JToggleButton();
			b9.setIcon(new ImageIcon(getClass().getResource("/imagenes/9.png")));
			b9.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			b9.setActionCommand("9");
		}
		return b9;
	}

	public Cronometro getCronometro() {
		if(cronometro == null){
			cronometro = new Cronometro();
			cronometro.getMomentoActual().setFont(new Font("Tahoma",Font.BOLD,18));
		}
		return cronometro;
	}

	public JRadioButton getDificultadDificil() {
		if(dificultadDificil == null){
			dificultadDificil = new JRadioButton();
			dificultadDificil.setText("Dificil");
			dificultadDificil.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return dificultadDificil;
	}

	public JRadioButton getDificultadFacil() {
		if(dificultadFacil == null){
			dificultadFacil = new JRadioButton();
			dificultadFacil.setText("Facil");
			dificultadFacil.setFont(new Font("Tahoma", Font.PLAIN, 12));
			dificultadFacil.setSelected(true);
		}
		return dificultadFacil;
	}

	public JRadioButton getDificultadMedio() {
		if(dificultadMedio == null){
			dificultadMedio = new JRadioButton();
			dificultadMedio.setText("Media");
			dificultadMedio.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return dificultadMedio;
	}

	public JRadioButton getDificultadMuyDificil() {
		if(dificultadMuyDificil == null){
			dificultadMuyDificil = new JRadioButton();
			dificultadMuyDificil.setText("Muy Dificil");
			dificultadMuyDificil.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return dificultadMuyDificil;
	}

	private JLabel getDifucultad() {
		if(difucultad == null){
			difucultad = new JLabel();
			difucultad.setText("DIFICULTAD");
			difucultad.setFont(new Font("Tahoma", Font.BOLD, 18));
			difucultad.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return difucultad;
	}

	public JButton getNuevo() {
		if(nuevo == null){
			nuevo = new JButton();
			nuevo.setText("Nuevo");
			nuevo.setFont(new Font("Tahoma", Font.PLAIN, 12));
			nuevo.setToolTipText("Inicia nuevo juego");
		}
		return nuevo;
	}

	private JPanel getPanelSur() {
		if(panelSur == null){
			panelSur = new JPanel();
			panelSur.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			GridLayout layout = new GridLayout();
			layout.setRows(3);
			JPanel panel1 = new JPanel();
			panel1.setLayout(new GridLayout());
			panel1.add(getRevisar());
			JPanel panel2 = new JPanel();
			panel2.setLayout(new GridLayout());
			panel2.add(getNuevo());
			panel2.add(getReiniciar());
			JPanel panel3 = new JPanel();
			panel3.setLayout(new GridLayout());
			panel3.add(getPausa());
			panelSur.setLayout(layout);
			panelSur.add(panel1);
			panelSur.add(panel2);
			panelSur.add(panel3);
		}
		return panelSur;
	}

	private JPanel getPanelNorte() {
		if(panelNorte == null){
			panelNorte = new JPanel();
			panelNorte.setPreferredSize(new Dimension(100,168));
			panelNorte.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED), BorderFactory.createBevelBorder(BevelBorder.LOWERED)));
			GridLayout layout = new GridLayout();
			layout.setRows(3);
			layout.setColumns(3);
			layout.setHgap(2);
			layout.setVgap(2);
			panelNorte.setLayout(layout);
			panelNorte.add(getB1());
			panelNorte.add(getB2());
			panelNorte.add(getB3());
			panelNorte.add(getB4());
			panelNorte.add(getB5());
			panelNorte.add(getB6());
			panelNorte.add(getB7());
			panelNorte.add(getB8());
			panelNorte.add(getB9());
		}
		return panelNorte;
	}

	private JPanel getPanelCentral() {
		if(panelCentral == null){
			panelCentral = new JPanel();			
			panelCentral.setLayout(new BorderLayout());
			panelCentral.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			
			JPanel panel1 = new JPanel();
			GridLayout layout1 = new GridLayout();
			layout1.setColumns(1);
			layout1.setRows(6);
			panel1.setLayout(layout1);
			panel1.add(new JLabel(""));
			panel1.add(getDifucultad());
			panel1.add(getDificultadFacil());
			panel1.add(getDificultadMedio());
			panel1.add(getDificultadDificil());
			panel1.add(getDificultadMuyDificil());
			
			JPanel panel2 = new JPanel();
			GridLayout layout2 = new GridLayout();
			layout2.setColumns(1);
			layout2.setRows(3);
			panel2.setLayout(layout2);
			panel2.add(getTiempo());
			panel2.add(getCronometro());
			
			panelCentral.add(panel1,BorderLayout.NORTH);
			panelCentral.add(panel2,BorderLayout.SOUTH);
		}
		return panelCentral;
	}

	public JButton getReiniciar() {
		if(reiniciar == null){
			reiniciar = new JButton();
			reiniciar.setText("Reiniciar");
			reiniciar.setFont(new Font("Tahoma", Font.PLAIN, 12));
			reiniciar.setToolTipText("Reinicia el mismo juego");
			reiniciar.setEnabled(false);
		}
		return reiniciar;
	}

	public JButton getRevisar() {
		if(revisar == null){
			revisar = new JButton();
			revisar.setText("Revisar");
			revisar.setFont(new Font("Tahoma", Font.PLAIN, 12));
			revisar.setToolTipText("Revisa el Juego, pero deben\nestar todas las casillas llenas");
			revisar.setEnabled(false);
		}
		return revisar;
	}
	
	public JButton getPausa(){
		if(pausa == null){
			pausa = new JButton();
			pausa.setText("Pausar");
			pausa.setFont(new Font("Tahoma",Font.PLAIN,12));
			pausa.setEnabled(false);
		}
		return pausa;
	}

	private JLabel getTiempo() {
		if(tiempo == null){
			tiempo = new JLabel();
			tiempo.setText("TIEMPO");
			tiempo.setFont(new Font("Tahoma", Font.BOLD, 18));
			tiempo.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return tiempo;
	}	
}  //  @jve:decl-index=0:visual-constraint="10,10" 
