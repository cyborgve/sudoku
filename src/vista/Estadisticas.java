package vista;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;

import controlador.ControladorEstadisticas;

public class Estadisticas extends JDialog {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JPanel botones = null;
	private JToggleButton facil = null;
	private JToggleButton medio = null;
	private JToggleButton dificil = null;
	private JToggleButton muyDificil = null;
	private ControladorEstadisticas controlador = null;
	private JScrollPane jScrollPane = null;
	private JTable tablaEstadisticas = null;
	
	public Estadisticas() {
		initialize();
		this.controlador = new ControladorEstadisticas(this);
		this.asignarControlador();
	}

	private void initialize() {
		this.setSize(400, 243);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setContentPane(getJContentPane());
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setTitle("Estadisticas");
		this.setResizable(false);
		this.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
					dispose();
				}
			}			
		});
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
			jContentPane.add(getBotones(), BorderLayout.NORTH);
			jContentPane.add(getJScrollPane(), BorderLayout.CENTER);
		}
		return jContentPane;
	}

	private JPanel getBotones() {
		if (botones == null) {
			botones = new JPanel();
			botones.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
			GridLayout layout = new GridLayout();
			layout.setRows(1);
			layout.setHgap(1);			
			botones.setLayout(layout);
			botones.add(getFacil(), null);
			botones.add(getMedio(), null);
			botones.add(getDificil(), null);
			botones.add(getMuyDificil(), null);
		}
		return botones;
	}

	public JToggleButton getFacil() {
		if (facil == null) {
			facil = new JToggleButton();
			facil.setText("Facil");
			facil.setFont(new Font("Tahoma", Font.BOLD, 11));
		}
		return facil;
	}

	public JToggleButton getMedio() {
		if (medio == null) {
			medio = new JToggleButton();
			medio.setText("Medio");
			medio.setFont(new Font("Tahoma", Font.BOLD, 11));
		}
		return medio;
	}

	public JToggleButton getDificil() {
		if (dificil == null) {
			dificil = new JToggleButton();
			dificil.setText("Dificil");
			dificil.setFont(new Font("Tahoma", Font.BOLD, 11));
		}
		return dificil;
	}

	public JToggleButton getMuyDificil() {
		if (muyDificil == null) {
			muyDificil = new JToggleButton();
			muyDificil.setText("Muy Dificil");
			muyDificil.setFont(new Font("Tahoma", Font.BOLD, 11));
		}
		return muyDificil;
	}

	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getTablaEstadisticas());
			jScrollPane.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		}
		return jScrollPane;
	}

	public JTable getTablaEstadisticas() {
		if (tablaEstadisticas == null) {
			tablaEstadisticas = new JTable();
		}
		return tablaEstadisticas;
	}
	
	private void asignarControlador(){
		getFacil().addActionListener(controlador);
		getMedio().addActionListener(controlador);
		getDificil().addActionListener(controlador);
		getMuyDificil().addActionListener(controlador);
	}
	
	public void actualizarTabla(){
		jScrollPane.setViewportView(null);
		tablaEstadisticas = null;
		jScrollPane.setViewportView(getTablaEstadisticas());		
	}
}
