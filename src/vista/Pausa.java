package vista;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Pausa extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	private JLabel label = null;
	private Thread hilo = null;
	private boolean continuar;
	
	public Pausa(){
		this.continuar = true;
		initialize();
		this.hilo = new Thread(this);
		hilo.start();
	}
	
	private void initialize(){
		this.setLayout(new BorderLayout());
		this.add(getLabel(),BorderLayout.CENTER);
		
	}
	
	public JLabel getLabel(){
		if(label == null){
			label = new JLabel();
			label.setText("Juego en Pausa");
			label.setHorizontalTextPosition(SwingConstants.CENTER);
			label.setHorizontalAlignment(SwingConstants.CENTER);
		}
		return label;
	}
	
	public void setContinuar(boolean arg0){
		continuar = arg0;
	}
	
	

	public void run() {
		while(continuar){
			if(getLabel().getText().equalsIgnoreCase(""))getLabel().setText("Juego en Pausa");
			else getLabel().setText("");
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
