package vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.SoftBevelBorder;

public class AcercaDe extends JDialog {

	private static final long serialVersionUID = 1L;

	private JPanel jContentPane = null;
	private JLabel jLabel = null;
	private JPanel jPanel = null;
	private JLabel jLabel1 = null;
	private JLabel jLabel2 = null;
	private JLabel jLabel3 = null;
	private JLabel jLabel4 = null;

	public AcercaDe(Frame owner) {
		super(owner);
		initialize();
	}

	private void initialize() {
		this.setSize(300, 135);
		this.setContentPane(getJContentPane());
		this.setVisible(true);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.setTitle("Acerca de");
		this.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ESCAPE || e.getKeyCode() == 10){
					dispose();
				}
			}			
		});
	}

	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jLabel = new JLabel();
			jLabel.setFont(new Font("Tahoma",Font.PLAIN,20));
			jLabel.setForeground(Color.BLUE);
			jLabel.setText("Sudoku Diabolico 1.0");
			jLabel.setHorizontalTextPosition(SwingConstants.CENTER);
			jLabel.setHorizontalAlignment(SwingConstants.CENTER);
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.setBorder(new SoftBevelBorder(SoftBevelBorder.RAISED));
			jContentPane.add(jLabel, BorderLayout.NORTH);
			jContentPane.add(getJPanel(), BorderLayout.CENTER);
			jContentPane.addMouseListener(new MouseAdapter(){
				public void mouseClicked(MouseEvent e) {
					dispose();
				}				
			});
		}
		return jContentPane;
	}

	private JPanel getJPanel() {
		if (jPanel == null) {
			jLabel4 = new JLabel();
			jLabel4.setText("Octubre 2010");
			jLabel4.setHorizontalAlignment(SwingConstants.CENTER);
			jLabel4.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jLabel3 = new JLabel();
			jLabel3.setText("Barquisimeto - Venezuela");
			jLabel3.setHorizontalAlignment(SwingConstants.CENTER);
			jLabel3.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jLabel2 = new JLabel();
			jLabel2.setText("Bajo Licencia Freeware");
			jLabel2.setHorizontalAlignment(SwingConstants.CENTER);
			jLabel2.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jLabel1 = new JLabel();
			jLabel1.setBounds(new Rectangle(76, 20, 38, 16));
			jLabel1.setHorizontalAlignment(SwingConstants.CENTER);
			jLabel1.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jLabel1.setText("Creado por: Richard Iribarren");
			jPanel = new JPanel();
			GridLayout layout = new GridLayout();
			layout.setRows(4);
			jPanel.setLayout(layout);
			jPanel.add(jLabel1, null);
			jPanel.add(jLabel2, null);
			jPanel.add(jLabel3, null);
			jPanel.add(jLabel4, null);
		}
		return jPanel;
	}
} 
