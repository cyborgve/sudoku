package vista;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import controlador.ControladorPrincipal;



public class Principal extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel jContentPane = null;
	private Tablero tablero = null;
	private Herramientas herramientas = null;
	private JMenuBar jJMenuBar = null;
	private JMenu jMenuArchivo = null;
	private JMenu jMenuAyuda = null;
	private ControladorPrincipal arg0 = null;
	private Pausa pausa = null;

	private JMenuItem jmiEstadisticas = null;

	private JMenuItem jmiAcercaDe = null;
	

	public Principal() {
		super();
		this.arg0 = new ControladorPrincipal(this);
		agregarControladoresHerramientas();
		agregarControladoresTablero();
		initialize();
	}

	private void initialize() {
		this.setJMenuBar(getJJMenuBar());
		this.setContentPane(getJContentPane());
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				Principal thisClass = new Principal();
				thisClass.setSize(748, 559);
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setTitle("Sudoku Diabolico Beta");
				thisClass.setLocationRelativeTo(null);
				thisClass.setResizable(false);
				thisClass.setVisible(true);
			}
		});
	}

	public JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getTablero(), BorderLayout.CENTER);
			jContentPane.add(getHerramientas(), BorderLayout.EAST);
		}
		return jContentPane;
	}
	
	public Tablero getTablero(){
		if(tablero == null){
			tablero = new Tablero();
			tablero.setPreferredSize(new Dimension(558, 142));
		}
		return tablero;
	}

	private JMenuBar getJJMenuBar() {
		if (jJMenuBar == null) {
			jJMenuBar = new JMenuBar();
			jJMenuBar.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jJMenuBar.add(getJMenuArchivo());
			jJMenuBar.add(getJMenuAyuda());
		}
		return jJMenuBar;
	}

	private JMenu getJMenuArchivo() {
		if (jMenuArchivo == null) {
			jMenuArchivo = new JMenu();
			jMenuArchivo.setText("Archivo");
			jMenuArchivo.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jMenuArchivo.add(getJmiEstadisticas());
		}
		return jMenuArchivo;
	}

	private JMenu getJMenuAyuda() {
		if (jMenuAyuda == null) {
			jMenuAyuda = new JMenu();
			jMenuAyuda.setText("Ayuda");
			jMenuAyuda.setFont(new Font("Tahoma", Font.PLAIN, 12));
			jMenuAyuda.add(getJmiAcercaDe());
		}
		return jMenuAyuda;
	}
	
	public Herramientas getHerramientas(){
		if(herramientas == null){
			herramientas = new Herramientas();
			herramientas.setPreferredSize(new Dimension(175, 238));
		}
		return herramientas;
	}
	
	private Pausa getPausa(){
		if(pausa == null){
			pausa = new Pausa();
			pausa.getLabel().setFont(new Font("Tahoma",Font.BOLD,40));
		}
		return pausa;
	}
	
	/**
	 * This method initializes jmiEstadisticas	
	 * 	
	 * @return javax.swing.JMenuItem	
	 */
	private JMenuItem getJmiEstadisticas() {
		if (jmiEstadisticas == null) {
			jmiEstadisticas = new JMenuItem();
			jmiEstadisticas.setText("Estadisticas");
			jmiEstadisticas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		}
		return jmiEstadisticas;
	}

	private JMenuItem getJmiAcercaDe() {
		if (jmiAcercaDe == null) {
			jmiAcercaDe = new JMenuItem();
			jmiAcercaDe.setFont(new Font("Tahoma",Font.PLAIN,12));
			jmiAcercaDe.setText("Acerca de");
		}
		return jmiAcercaDe;
	}
	
	public void agregarControladoresHerramientas(){
		getJmiEstadisticas().addActionListener(arg0);
		getJmiAcercaDe().addActionListener(arg0);
		
		getHerramientas().getB1().addActionListener(arg0);
		getHerramientas().getB2().addActionListener(arg0);
		getHerramientas().getB3().addActionListener(arg0);
		getHerramientas().getB4().addActionListener(arg0);
		getHerramientas().getB5().addActionListener(arg0);
		getHerramientas().getB6().addActionListener(arg0);
		getHerramientas().getB7().addActionListener(arg0);
		getHerramientas().getB8().addActionListener(arg0);
		getHerramientas().getB9().addActionListener(arg0);
		
		getHerramientas().getRevisar().addActionListener(arg0);
		getHerramientas().getNuevo().addActionListener(arg0);
		getHerramientas().getReiniciar().addActionListener(arg0);
		getHerramientas().getPausa().addActionListener(arg0);
		
		getHerramientas().getDificultadFacil().addActionListener(arg0);
		getHerramientas().getDificultadMedio().addActionListener(arg0);
		getHerramientas().getDificultadDificil().addActionListener(arg0);
		getHerramientas().getDificultadMuyDificil().addActionListener(arg0);
	}
	
	public void agregarControladoresTablero(){
		getTablero().getA1().addActionListener(arg0);
		getTablero().getA2().addActionListener(arg0);
		getTablero().getA3().addActionListener(arg0);
		getTablero().getA4().addActionListener(arg0);
		getTablero().getA5().addActionListener(arg0);
		getTablero().getA6().addActionListener(arg0);
		getTablero().getA7().addActionListener(arg0);
		getTablero().getA8().addActionListener(arg0);
		getTablero().getA9().addActionListener(arg0);
		
		getTablero().getB1().addActionListener(arg0);
		getTablero().getB2().addActionListener(arg0);
		getTablero().getB3().addActionListener(arg0);
		getTablero().getB4().addActionListener(arg0);
		getTablero().getB5().addActionListener(arg0);
		getTablero().getB6().addActionListener(arg0);
		getTablero().getB7().addActionListener(arg0);
		getTablero().getB8().addActionListener(arg0);
		getTablero().getB9().addActionListener(arg0);
		
		getTablero().getC1().addActionListener(arg0);
		getTablero().getC2().addActionListener(arg0);
		getTablero().getC3().addActionListener(arg0);
		getTablero().getC4().addActionListener(arg0);
		getTablero().getC5().addActionListener(arg0);
		getTablero().getC6().addActionListener(arg0);
		getTablero().getC7().addActionListener(arg0);
		getTablero().getC8().addActionListener(arg0);
		getTablero().getC9().addActionListener(arg0);
		
		getTablero().getD1().addActionListener(arg0);
		getTablero().getD2().addActionListener(arg0);
		getTablero().getD3().addActionListener(arg0);
		getTablero().getD4().addActionListener(arg0);
		getTablero().getD5().addActionListener(arg0);
		getTablero().getD6().addActionListener(arg0);
		getTablero().getD7().addActionListener(arg0);
		getTablero().getD8().addActionListener(arg0);
		getTablero().getD9().addActionListener(arg0);
		
		getTablero().getE1().addActionListener(arg0);
		getTablero().getE2().addActionListener(arg0);
		getTablero().getE3().addActionListener(arg0);
		getTablero().getE4().addActionListener(arg0);
		getTablero().getE5().addActionListener(arg0);
		getTablero().getE6().addActionListener(arg0);
		getTablero().getE7().addActionListener(arg0);
		getTablero().getE8().addActionListener(arg0);
		getTablero().getE9().addActionListener(arg0);
		
		getTablero().getF1().addActionListener(arg0);
		getTablero().getF2().addActionListener(arg0);
		getTablero().getF3().addActionListener(arg0);
		getTablero().getF4().addActionListener(arg0);
		getTablero().getF5().addActionListener(arg0);
		getTablero().getF6().addActionListener(arg0);
		getTablero().getF7().addActionListener(arg0);
		getTablero().getF8().addActionListener(arg0);
		getTablero().getF9().addActionListener(arg0);
		
		getTablero().getG1().addActionListener(arg0);
		getTablero().getG2().addActionListener(arg0);
		getTablero().getG3().addActionListener(arg0);
		getTablero().getG4().addActionListener(arg0);
		getTablero().getG5().addActionListener(arg0);
		getTablero().getG6().addActionListener(arg0);
		getTablero().getG7().addActionListener(arg0);
		getTablero().getG8().addActionListener(arg0);
		getTablero().getG9().addActionListener(arg0);
		
		getTablero().getH1().addActionListener(arg0);
		getTablero().getH2().addActionListener(arg0);
		getTablero().getH3().addActionListener(arg0);
		getTablero().getH4().addActionListener(arg0);
		getTablero().getH5().addActionListener(arg0);
		getTablero().getH6().addActionListener(arg0);
		getTablero().getH7().addActionListener(arg0);
		getTablero().getH8().addActionListener(arg0);
		getTablero().getH9().addActionListener(arg0);
		
		getTablero().getI1().addActionListener(arg0);
		getTablero().getI2().addActionListener(arg0);
		getTablero().getI3().addActionListener(arg0);
		getTablero().getI4().addActionListener(arg0);
		getTablero().getI5().addActionListener(arg0);
		getTablero().getI6().addActionListener(arg0);
		getTablero().getI7().addActionListener(arg0);
		getTablero().getI8().addActionListener(arg0);
		getTablero().getI9().addActionListener(arg0);		
	}
	
	public void borrarTablero(){
		jContentPane.remove(getTablero());
		tablero = null;
		jContentPane.add(getTablero());
		agregarControladoresTablero();
	}
	
	public void pausar(boolean arg0){
		if(arg0){
			jContentPane.remove(getTablero());
			jContentPane.add(getPausa(),BorderLayout.CENTER);
		}else{
			getPausa().setContinuar(false);
			jContentPane.remove(getPausa());			
			pausa = null;
			jContentPane.add(getTablero(),BorderLayout.CENTER);
			this.repaint();
		}
	}
}